\documentclass{article}

\usepackage[sectionClearpage=false,images=true]{npStyle}

\usepackage[
backend=biber
%,style=authoryear
%,autocite=inline
%,url=true
%,doi=false
%,eprint=false
%,sorting=none
,style=numeric
]{biblatex}
\usepackage{csquotes}
\addbibresource{pimc.bib}

\newcommand{\red}[1]{\textcolor{red}{#1}}

\title{
    Modulo 3 \\
    \Large{PIMC per la particella sul cerchio}
}
\author{Alessandro Piazza \and Porciani Niccolo'}

\begin{document}

\maketitle
\tableofcontents

\section{Particella sul cerchio}
Consideriamo il sistema quantistico di una particella di massa \( m \) libera su una circonferenza di raggio \( R \). I livelli energetici sono discreti e doppiamente degeneri, dati da \( E_{n} = \hbar^{2} n^{2} / 2mR^{2}  \), per cui la funzione di partizione è calcolabile esattamente
\begin{equation}
    Z(\beta) = \sum_{n \in \Z} \exp{\left(-\beta \frac{\hbar^{2} n^{2}}{2mR^{2}}\right)}
\end{equation}
Nel formalismo del path integral la funzione di partizione è
\begin{equation}
    Z(\beta) = \mathcal{N} \int \mathcal{D}[x(\tau)] \, e^{-S_{E}[x] / \hbar} \qquad S_{E}[x] = \int_{0}^{\beta\hbar} \frac{m}{2} \left(\dot{x}(\tau)\right)^{2} \dif{\tau}
\end{equation}
dove l'integrale è fatto su tutti i cammini periodici con immagine nel cerchio \( \S_{R}^{1} \). La condizione di periodicità sul cerchio può essere realizzata considerando una coordinata \( x \in \R \) e identificando punti che differiscono di \( 2\pi R \). Ciò identifica naturalmente classi distinte di cammini che partono da \( x(0) = x_{0} \) e terminano in \( x(\beta \hbar) = x_{0} + 2\pi R Q \) al variare di \( Q \in \Z \) essendo quindi \( x(\beta \hbar) \) identificato con \( x_{0} \) su \( \S_{R}^{1} \). \( Q \) è detto \emph{winding number} essendo il numero di volte che il cammino si avvolge sul cerchio (con segno) e identifica il ``settore topologico'' o classe di omotopia del cammino. La funzione di partizione si riscrive quindi come
\begin{equation}
    Z(\beta) = \sum_{Q \in \Z} Z_{Q}(\beta) \qquad Z_{Q}(\beta) = \mathcal{N} \int\displaylimits_{x(\beta \hbar) = x(0) + 2\pi R Q} \mathcal{D}[x(\tau)] \, e^{-S_{E}[x] / \hbar}
\end{equation}
A fisso settore topologico dobbiamo calcolare il path integral di una particella libera con condizioni al bordo non periodiche che è semplicemente
\begin{equation}
    Z_{Q}(\beta) \propto e^{-S_{E}[x\ped{cl}]} = \exp\left(- \frac{1}{\hbar} \frac{(2\pi R Q)^{2}}{2 \beta\hbar}\right)
\end{equation}
Così infine
\begin{equation}\label{eq:partition-function-Q}
    Z(\beta) = \frac{1}{\sqrt{2\pi \beta \hbar \chi}}\sum_{Q \in \Z} \exp\left(-\frac{Q^{2}}{2\beta \hbar \chi}\right) \qquad \chi = \frac{\hbar}{4\pi^{2} m R^{2}}
\end{equation}
Confrontando le due espressioni della funzione di partizione abbiamo che:
\begin{itemize}
    \item ad alta temperatura \( \beta \hbar \chi \ll 1 \) contribuiscono solo i livelli energetici a \( \abs{n} \) piccolo, mentre contribuiscono cammini con winding number arbitrariamente alto (la distribuzione in \( Q \) è circa gaussiana con deviazione standard \( \sqrt{\beta \hbar \chi} \)). Il tempo euclideo \( \beta \hbar \) è infatti ``lungo'' e quindi i cammini riescono ad avvolgersi molte volte;
    \item a bassa temperatura \( \beta \hbar \chi \gg 1 \) contribuiscono tutti i livelli energetici ma solo i cammini con winding number piccolo.
\end{itemize}

\section{Simulazioni}

\subsection{Discretizzazione}

L'azione discretizzata per il time-lattice della particella sul cerchio è
\begin{equation}
    \frac{S_{E, L}}{\hbar} = \frac{1}{2\eta} \sum_{i=1}^{N}d(y_{i+1}, y_{i})^{2} \qquad \eta = a \chi
\end{equation}
dove \( y_{i} \in [0, 1) \), \( y_{N+1} = y_{1} \) e \( d \) è la distanza geodetica con segno sul cerchio
\begin{equation}
    d(y, x) =
    \begin{cases}
        y - x & \text{se } |y - x| \leq 1/2 \\
        y - x - 1 & \text{se } y - x > 1/2 \\
        y - x + 1 & \text{se } y - x < -1/2
    \end{cases}
\end{equation}
La relazione tra \( \eta \), \( N \) e la temperatura (o tempo euclideo) è
\begin{equation}
    N \eta = \beta \hbar \chi
\end{equation}
L'energia del cammino, identificata dalla relazione \( \beta E = S_{E, L}/\hbar \), è
\begin{equation}
    E = \frac{1}{2\eta \hbar \beta} \sum_{i=1}^{N} d(y_{i+1}, y_{i})^{2} = \frac{\chi}{2N\eta^{2}} \sum_{i=1}^{N} d(y_{i+1}, y_{i})^{2}
\end{equation}
Se consideriamo che due punti successivi del cammino siano sempre collegati dall'arco di lunghezza minima la carica topologica è invece data da
\begin{equation}
    Q = \sum_{i=1}^{N} d(y_{i+1}, y_{i})
\end{equation}

\subsection{Freezing e algoritmi di simulazione}

Se vogliamo simulare la termodinamica della particella sul cerchio tramite PIMC possiamo utilizzare l'algoritmo di Metropolis. Consideriamo come ``passo elementare'' l'update del sito \( i \)-esimo, in cui estraiamo un numero casuale \( \Delta y \) distribuito uniformemente nell'intervallo \( [y_{i} - \delta, y_{i} + \delta] \) e proponiamo come nuovo valore\footnote{Con \( \{x\} \) indichiamo la parte frazionaria di \( x \)} \( y'_{i} = \{y_{i} + \Delta y\} \), da accettare o rigettare come usuale. Per mantenere un'accettanza costante al variare di \( \eta \) nel limite continuo \( \eta \to 0 \), ci aspettiamo di dover scegliere \( \delta = \sqrt{\eta} \), e prendiamo inoltre come ``passo'' dell'algoritmo la ripetizione di \( N \) passi elementari.\\

Procedendo in questo modo ci si imbatte tuttavia in un fenomeno di cosiddetto ``freezing'' della carica topologica, in cui la catena perde di ergodicità e non riproduce più fedelmente il limite continuo della teoria a tempo Montecarlo finito. Ciò accade in quanto nella versione discreta il valore di \( Q \) può cambiare solo quando la distanza tra due punti consecutivi \( y_{i} \) e \( y_{i+1} \) ``supera'' \( 1/2 \), ovvero quando cambia l'arco associato alla distanza geodetica. Si ha però che la varianza di \( \Delta y_j = y_{j+1} - y_j \) è approssimativamente \( \eta \), e il limite continuo corrisponde proprio a \( \eta \to 0 \), rendendo quindi estremamente improbabile che \( \Delta y_j \) diventi comparabile con \( 1/2 \).

Il fenomeno del freezing sarà particolarmente evidente a bassa temperatura, quando alla funzione di partizione contribuiscono cammini con \( Q \) arbitrariamente elevato, mentre ad alta temperatura, quando è rilevante solo il settore \( Q = 0 \), il fenomeno sarà evidente solo se il cammino è inizializzato ad un valore di \( Q \neq 0 \) che rimarrà costante nell'arco della simulazione.\\

Per ovviare a questi problemi abbiamo provato alcune varianti dell'usuale algoritmo di Metropolis
\begin{itemize}
    \item \emph{algoritmo Metropolis con \( \delta = 1/2 \)}: se nell'update Metropolis proponiamo un update in \( [y_{i}-1/2, y_{i}+1/2] \) ci aspettiamo di riuscire a cambiare più facilmente settore topologico e in linea di principio a campionare bene la distribuzione di \( Q \). Tuttavia così facendo ci aspettiamo di avere un'accettanza della mossa Metropolis che tende a 0 al diminuire di \( \eta \)
    \item \emph{algoritmo Metropolis con \( \delta = \sqrt{\eta} \)} combinato con la \emph{mossa del sarto}: possiamo combinare l'algoritmo di Metropolis con \( \delta = \sqrt{\eta} \) con una mossa, microcanonica nel limite continuo, in cui consideriamo il primo punto \( y_{i_0} \) che si allontani di \( 1/2 \) da \( y_1 \) e riflettiamo la sezione di cammino con \( i \geq i_0 \) attorno a \( y_1 + 1/2 \) stesso. Nel caso discreto, fissata una tolleranza \( \epsilon \) (che tipicamente scala con \( \eta \)) e posto \( y_{r} = \{y_{0} + 1/2\} \) cerchiamo il minimo \( i_{0} \) tale che
    \begin{equation}
        \abs{d(y_{i_{0}}, y_{r})} \leq \epsilon
    \end{equation}
    e proponiamo come update
    \begin{equation}
        y_{i}' = \{2 y_{r} - y_{i}\} \qquad \forall i = i_{0}, \cdots, N
    \end{equation}
    Nel caso discreto tale mossa non è microcanonica ma porta ad una variazione dell'azione locale
    \begin{equation}
        \Delta S_{E, L} = \frac{1}{2\eta} \left(d(y_{i_{0}}', y_{i_{0}-1})^{2} - d(y_{i_{0}}, y_{i_{0}-1})^{2}\right)
    \end{equation}
    per cui l'update (non locale) andrà accettato o rifiutato con l'usuale accettanza Metropolis. Quando riflettiamo il cammino stiamo cambiando la carica topologica \( Q \to -Q + 1 \) oppure \( Q \to -Q -1 \) in base alla posizione di \( y_{i_{0}} \) e questo ci permette di aggirare i problemi di non ergodicità.

    La mossa del sarto viene proposta con probabilità fissata \( p\ped{cut} \) dopo la quale vengono eseguiti \( N \) update Metropolis su siti estratti casualmente in modo uniforme.

    % \item \emph{algoritmo Metropolis con \(  \)} combinato con la \emph{mossa del sarto}
\end{itemize}

\subsection{Implementazione}
Gli algoritmi esposti sono stati implementati in \texttt{C++} e sono disponibili al repository \href{https://gitlab.com/niccolo-porciani/numerici-progetto-pimc}{numerici-progetto-pimc}, in particolare in \texttt{simulazioni/particle.cpp}, dove sono anche disponibili gli script per l'analisi delle simulazioni e delle osservabili di interesse.

\subsection{Parametri e dati raccolti}
Per le simulazioni abbiamo lavorato in unità di \( \chi = \hbar = 1 \). Avendo molti parametri da regolare (\( N, \eta, \beta, \epsilon, p\ped{cut} \), tempo di simulazione Montecarlo, frequenza delle misure), sono state fatte alcune scelte in parte dettate dalla potenza di calcolo che avevamo a disposizione e in parte dai valori suggeriti in \cite{BonatiDElia}.

\begin{itemize}
    \item \( \beta = 10 \) in quanto siamo interessati a studiare il freezing topologico dovuto ai problemi del Metropolis nel limite continuo, quindi ci mettiamo ad alta temperatura in modo che esso sia evidente rispetto al valore fisico \( \sigma_Q \simeq \sqrt{\beta} \simeq 3 \);
    \item \( p\ped{cut} = 0.06 \) e frequenza di misura \( T\ped{sample} = 10 \) in modo da avere statisticamente almeno 1 misura ogni volta che si esegue la mossa del sarto;
    \item \( \epsilon = 0.02\, \eta \) come usato in \cite{BonatiDElia};
    \item \( 10^{-3} \leq \eta \leq 1/4 \) in modo che la larghezza della distribuzione uniforme in cui facciamo la proposta di update nel Metropolis non superi 1 e in modo da esplorare un'appropriata regione ad \( \eta \) piccoli;
    \item \( N\ped{step} = 5 \times 10^5 \) e \( T\ped{sample} = 1 \) per una simulazione preliminare e poi \( N\ped{step} = 6 \times 10^6 \) e \( T\ped{sample} = 5 \), scelto in modo da avere approssimativamente almeno \( 1200 \) tempi di correlazione per ogni simulazione\footnote{Si veda la sezione~\ref{sec:correlazioni} sui tempi di correlazione per un commento su come si è stimato tale valore}.
\end{itemize}

\noindent Per i vari algoritmi esposti in precedenza abbiamo studiato
\begin{enumerate}
    \item la dipendenza da \( \eta \) del tempo di autocorrelazione dell'energia e della carica topologica
    \item la distribuzione della carica topologica per alcuni valori esemplari di \( \eta \) che mostrino i limiti dell'algoritmo di Metropolis
    \item il limite al continuo della varianza di \( Q \) confrontato con il valore atteso \( \beta \)
%    \item \textcolor{red}{il limite al continuo del valore medio della carica topologica confrontato con il valore atteso \( \braket{Q} = 0 \) partendo da un cammino con \( Q \neq 0 \)}
\end{enumerate}

\section{Analisi dei risultati}

\subsection{Tempi di correlazione}\label{sec:correlazioni}

Dai dataset delle simulazioni preliminari  si sono estratte le correlazioni dei due osservabili \( O = E, Q \) (energia e carica topologica), come
\begin{equation}
    C_O(k) = \frac{1}{N - k} \sum_{i = 0}^{N - k} O({i})O({i+k}) - \del{\frac{1}{N} \sum_{i=0}^{N} O(i)}^2
\end{equation}
da cui si può estrarre un tempo di correlazione tramite un fit esponenziale
\begin{equation}
    C_O(k) = A_O\: e^{-k / \tau_O}
\end{equation}
Per i dettagli implementativi si veda \texttt{analisi/obs\_mc\_correlation.py}. I valori così ottenuti sono riportati nei grafici di Figura~\ref{fig:tau_vs_eta}.

\begin{figure}[h]
    \adjustbox{center}{
    % Figure prodotte da fit_mc_correlations.py sul dataset 6M
    \begin{tabular}{ccc}
        \small \hspace{3ex}Metropolis \( \delta = \sqrt{\eta} \)&
        \small \hspace{3ex}Metropolis \( \delta = 1/2 \)&
        \small \hspace{3ex}Metropolis con sarto\\
        \includegraphics[width=0.33\textwidth]{./staticimgs/correlazioni/corr_500k_metro_E.png} &
        \includegraphics[width=0.33\textwidth]{./staticimgs/correlazioni/corr_500k_half_E.png} &
        \includegraphics[width=0.33\textwidth]{./staticimgs/correlazioni/corr_500k_cut_E.png}\\
        \includegraphics[width=0.33\textwidth]{./staticimgs/correlazioni/corr_500k_metro_Q.png} &
        \includegraphics[width=0.33\textwidth]{./staticimgs/correlazioni/corr_500k_half_Q.png} &
        \includegraphics[width=0.33\textwidth]{./staticimgs/correlazioni/corr_500k_cut_Q.png}\\
    \end{tabular}
    }
    \caption{Tempi di correlazione stimati da fit esponenziali per \( E \) e \( Q \) nei tre algoritmi, in funzione di \( \eta \). Nei grafici di autocorrelazione della carica topologica i punti con \( \tau = 1 \) corrispondono ai dataset per cui il fit esponenziale non converge, in quanto il tempo di correlazione diventa comparabile o supera il tempo di simulazione e la carica topologica è costante, e il fit era inizializzato con \( \tau = 1 \). Si noti la scala verticale differente del grafico per l'algoritmo con mossa del sarto.}
    \label{fig:tau_vs_eta}
\end{figure}

\paragraph*{Simulazioni successive} I risultati così ottenuti per le correlazioni dell'energia sono poi stati utilizzati anche per stimare grossolanamente quale fosse il tempo di simulazione necessario per avere sempre un numero sufficiente di tempi di correlazione nelle simulazioni successive. I valori massimi dei tempi di correlazione fuori dalla regione di freezing sono rispettivamente
\begin{itemize}
    \item Metropolis \( \delta = \sqrt{\eta} \): \( \sim 1300 \div 2300 \) a \( \eta \simeq 0.008 \)
    \item Metropolis \( \delta = 1/2 \): \( \sim 1300 \div 5000 \) a \( \eta \simeq 0.005 \)
    \item Sarto: \( \sim 40 \div 130 \) a \( \eta \simeq 0.001 \)
\end{itemize}
Si è quindi scelto di prendere \( N\ped{step} = 6 \times 10^6 \) per le simulazioni successive, corrispondente ad almeno \( 1200 \) tempi di correlazione dell'energia in tutte le simulazioni.

\subsection{Freezing e carica topologica}

In tutta questa sezione ci riferiamo alle simulazioni con \( N\ped{step} = 6 \times 10^6 \).

\paragraph*{Tempi di correlazione} Dai grafici del tempo di correlazione della carica topologica di figura~\ref{fig:tau_vs_eta} è evidente il fenomeno del freezing: per gli algoritmi Metropolis semplici il tempo di correlazione diverge rapidamente andando verso \( \eta \) piccoli, fino a diventare comparabile con il tempo di simulazione. È viceversa evidente che l'algoritmo con la mossa del sarto risolve tale problema, in quanto il tempo di correlazione satura ad un valore ben minore del tempo di simulazione e rimane costante andando verso il limite continuo.

\begin{figure}[h]
    \adjustbox{center}{
        \begin{tabular}{cccc}
            \includegraphics[width=0.25\textwidth]{./staticimgs/istogrammi/istogramma_6M_metro_0.250.png} &
            \includegraphics[width=0.25\textwidth]{./staticimgs/istogrammi/istogramma_6M_metro_0.010.png} &
            \includegraphics[width=0.25\textwidth]{./staticimgs/istogrammi/istogramma_6M_metro_0.008.png} &
            \includegraphics[width=0.25\textwidth]{./staticimgs/istogrammi/istogramma_6M_metro_0.001.png} \\
            \includegraphics[width=0.25\textwidth]{./staticimgs/istogrammi/istogramma_6M_half_0.250.png} &
            \includegraphics[width=0.25\textwidth]{./staticimgs/istogrammi/istogramma_6M_half_0.006.png} &
            \includegraphics[width=0.25\textwidth]{./staticimgs/istogrammi/istogramma_6M_half_0.005.png} &
            \includegraphics[width=0.25\textwidth]{./staticimgs/istogrammi/istogramma_6M_half_0.001.png} \\
            \includegraphics[width=0.25\textwidth]{./staticimgs/istogrammi/istogramma_6M_cut_0.250.png} &
            \includegraphics[width=0.25\textwidth]{./staticimgs/istogrammi/istogramma_6M_cut_0.010.png} &
            \includegraphics[width=0.25\textwidth]{./staticimgs/istogrammi/istogramma_6M_cut_0.005.png} &
            \includegraphics[width=0.25\textwidth]{./staticimgs/istogrammi/istogramma_6M_cut_0.001.png} \\
    \end{tabular}
    }
    \caption{\label{fig:istogrammi_caratteristici} Istogrammi della carica topologica per diversi algoritmi (righe, dall'alto Metropolis \( \delta = \sqrt{\eta} \), Metropolis \( \delta = 1/2 \), Metropolis con sarto), a diversi valori di \( \eta \) (riportati nei singoli grafici). In verde è riportata la distribuzione prevista in approssimazione gaussiana, valida per la temperatura scelta.}
\end{figure}

\paragraph*{Istogrammi} In figura \ref{fig:istogrammi_caratteristici} riportiamo alcuni esempi significativi di istogrammi della carica topologica per i tre algoritmi e per diversi valori di \( \eta \). Includiamo in particolare per ogni algoritmo il massimo e minimo valore di \( \eta \) simulati; per i due algoritmi Metropolis l'ultimo valore incluso e il primo escluso nella regione di \( \eta \) piccolo nell'estrapolazione, e per l'algoritmo del sarto due valori intermedi.

Osserviamo che indipendentemente dall'algoritmo la distribuzione a \( \eta \simeq 0.25 \) risulta piuttosto distante da quella attesa nel limite continuo, compatibilmente con il fatto che tale valore di \( \eta \) non è sufficientemente piccolo ad evitare errori di discretizzazione. Per gli algoritmi Metropolis semplici osserviamo che andando verso \( \eta \) piccoli la distribuzione inizialmente migliora, come previsto, per poi peggiorare notevolmente quando si incontra la regione di freezing. In particolare ad \( \eta = 0.001 \) per questi due algoritmi il valore di \( Q \) risulta costante e pari al valore in cui viene inizializzato, coerentemente con un freezing completo. Per l'algoritmo con la mossa del sarto, viceversa, la distribuzione si stabilizza su quella attesa ad \( \eta \) arbitrariamente piccoli, confermando che il problema del freezing viene completamente rimosso da tale algoritmo.

\paragraph*{Varianza della carica topologica} Dall'equazione~\eqref{eq:partition-function-Q} ci aspettiamo nel limite continuo
\begin{equation}
    \mathrm{Var}(Q) = \beta
\end{equation}
essendo l'approssimazione gaussiana sostanzialmente indistinguibile dalla distribuzione discreta per \( \beta = 10 \). Per ognuno degli algoritmi si è calcolata la varianza della carica topologica nel range di \( \eta \) esplorato (\texttt{analisi/Q\_analysis.py}). L'errore è stato stimato con bootstrap con binning e la dimensione del blocco è stata fissata al raggiungimento qualitativo di un plateau nella stima dello stesso per il punto a \( \eta \) più piccolo considerato ``valido'' per il fit successivi (i grafici rilevanti sono in Figura~\ref{fig:bootstrap-optimize}).

\begin{figure}[h!]
    \centering
    \begin{subfigure}{.31\textwidth}
        \includegraphics[width=\textwidth]{./staticimgs/bootstrap_opt/metropolis_eta_bootstrap_opt_Q.png}
        \caption{Metropolis \( \delta = \sqrt{\eta} \)}
    \end{subfigure}
    \begin{subfigure}{.31\textwidth}
        \includegraphics[width=\textwidth]{./staticimgs/bootstrap_opt/metropolis_half_bootstrap_opt_Q.png}
        \caption{Metropolis \( \delta = 1/2 \)}
    \end{subfigure}
    \begin{subfigure}{.31\textwidth}
        \includegraphics[width=\textwidth]{./staticimgs/bootstrap_opt/cut_eta_bootstrap_opt_Q.png}
        \caption{Metropolis \( \delta = 1/2 \)}
    \end{subfigure}
    \caption{\label{fig:bootstrap-optimize} errore stimato con bootstrap con binning al variare della dimensione del blocco.}
\end{figure}

Per l'estrapolazione al continuo abbiamo provato con una dipendenza quadratica da
\begin{equation}
    f(\eta) = a + b \eta^{2}
\end{equation}
considerando \( a \) come una stima di \( \mathrm{Var}(Q) \).

Nel caso dell'algoritmo di Metropolis con \( \delta = \sqrt{\eta} \) e \( \delta = 1/2 \) si è fatto un fit da \( \eta = 0.06 \) (scelto guardando qualitativamente la stabilità del fit) all'\( \eta \) più piccolo per il quale la distribuzione della carica topologica fosse qualitativamente soddisfacente. Nel caso dell'algoritmo del Sarto possiamo usare tutti i valori di \( \eta \leq 0.06 \): tuttavia l'estrapolazione al continuo non sembra essere ben descritta dall'andamento quadratico e abbiamo quindi deciso di effettuare semplicemente un fit con costante sugli \( \eta \leq 0.023 \) esplorati. Per i dettagli si veda \texttt{analisi/var\_Q\_plot.py}.

Si sono ottenuti i seguenti risultati
\begin{itemize}
    \item \emph{Metropolis \( \delta = \sqrt{\eta} \)}: per l'errore sulla varianza si è scelta una dimensione del blocco di 6000. Si è poi effettuato un fit quadratico sugli  \( \eta \in [0.009, 0.06] \) ottenendo
    \begin{equation}
        a = 1.0253 \pm 0.0017 \qquad b = -69.87 \pm 0.92 \qquad \chi^{2}/\mathrm{ndof} = 1.8
    \end{equation}
    In Figura~\ref{fig:var-Q-metro} si riportano le stime della varianza e la curva di best fit. Anche da questo grafico si osserva il freezing della carica topologica \( \mathrm{Var}(Q) = 0 \) per \( \eta \lesssim 0.008 \)

    \item \emph{Metropolis \( \delta = 1/2 \)}: per l'errore sulla varianza si è scelta una dimensione del blocco di 3000. Si è poi effettuato un fit quadratico sugli  \( \eta \in [0.008, 0.06] \) ottenendo
    \begin{equation}
        a = 1.02250 \pm 0.00082 \qquad b = -68.79 \pm 0.50 \qquad \chi^{2}/\mathrm{ndof} = 4.7
    \end{equation}
    In Figura~\ref{fig:var-Q-half} si riportano le stime della varianza e la curva di best fit. Anche da questo grafico si osserva il freezing della carica topologica \( \mathrm{Var}(Q) = 0 \) per \( \eta \lesssim 0.005 \) (quindi un leggero ``guadagno'' rispetto al Metropolis standard)

    \item \emph{Sarto}: per l'errore sulla varianza si è scelta una dimensione del blocco di 3000. Effettuando un fit quadratico nel range \( \eta \in[0.006, 0.06] \) si ottengono i seguenti parametri
    \begin{equation}
        a = 1.0177 \pm 0.0013 \qquad b = -66.56 \pm 0.66 \qquad \chi^2 / \mathrm{ndof} = 3.72
    \end{equation}
    Si è poi effettuato un fit costante sugli \( \eta \in [0, 0.0023] \) ottenendo
    \begin{equation}
        a = 0.9951 \pm 0.0018 \qquad \chi^{2}/\mathrm{ndof} = 0.92
    \end{equation}
    In Figura~\ref{fig:var-Q-cut} si riportano le stime della varianza e la curva di best fit costante e quadratica. Come atteso, in questo caso non si osserva il fenomeno di freezing della carica topologica ma si osserva il raggiungimento di un plateau in scala semi-logaritmica in corrispondenza di \( \mathrm{Var}(Q) = \beta \).
\end{itemize}

Osserviamo che la stima migliore della varianza di \( Q \) (entro circa 2.5 deviazioni standard dal valore atteso) si ottiene dal fit costante fatto sui dati ottenuti tramite l'algoritmo del sarto, che, come atteso, permette di aggirare i problemi dovuti al freezing della carica topologica e di spingersi quindi a valori di \( \eta \) più piccoli.

Con il fit quadratico otteniamo delle stime sistematicamente superiori al valore atteso e dei \( \chi^{2} \) ridotti abbastanza elevati. Ciò potrebbe essere dovuto al fatto che il limite continuo non è ben descritto da un andamento quadratico, cosa che viene suggerita anche dal fatto che il fit quadratico nel caso dell'algoritmo del sarto sovrastima quasi tutti i punti ottenuti agli \( \eta \) più piccoli. Tuttavia, come già detto, il Metropolis da solo non permette di esplorare la zona a \( \eta \) sufficientemente piccolo e risulta quindi impraticabile effettuare un fit costante su un numero sufficiente di punti.

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.7]{./staticimgs/varQ/metropolis_eta_var_Q.png}
    \caption{\label{fig:var-Q-metro} varianza della carica topologica con algoritmo Metropolis \( \delta = \sqrt{\eta} \).}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.7]{./staticimgs/varQ/metropolis_half_var_Q.png}
    \caption{\label{fig:var-Q-half} varianza della carica topologica con algoritmo Metropolis \( \delta = 1/2 \).}
\end{figure}

\begin{figure}[h!]
    \centering
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=0.7\textwidth]{./staticimgs/varQ/cut_eta_var_Q_quadratic.png}
        \caption{fit quadratico}
    \end{subfigure}
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=0.7\textwidth]{./staticimgs/varQ/cut_eta_var_Q.png}
        \caption{fit costante}
    \end{subfigure}
    \caption{\label{fig:var-Q-cut} varianza della carica topologica con l'algoritmo del sarto.}
\end{figure}

\subsection{Accettanza}
Come ultima verifica abbiamo confrontato l'accettanza delle mosse Metropolis nel caso \( \delta = \sqrt{\eta} \) e nel caso \( \delta = 1/2 \) (il caso del sarto è identico a Metropolis \( \delta = \sqrt{\eta} \)) al variare di \( \eta \). Le simulazioni usate sono le stesse simulazioni preliminari usate per la stima dei tempi di correlazione e i grafici relativi sono riportati in Figura~\ref{fig:acceptance}. Come atteso nel primo caso si raggiunge un plateau, mentre nel secondo caso l'accettanza va a zero (con un andamento che sembra essere una legge di potenza per \( \eta \leq 0.05 \)). Ciò spiega perché l'algoritmo con \( \delta = 1/2 \) non presenta un miglioramento significativo rispetto a quello con \( \delta = \sqrt{\eta} \): per quanto sia possibile proporre una singola mossa che porta a una variazione della carica topologica, l'accettanza di tale mossa tende a 0 nel limite continuo.

\begin{figure}[h!]
    \centering
    \begin{subfigure}{.49\textwidth}
        \includegraphics[width=\textwidth]{./staticimgs/acceptance/metropolis_eta_acceptance.png}
        \caption{Metropolis \( \delta = \sqrt{\eta} \)}
    \end{subfigure}\hspace{0.019\textwidth}%
    \begin{subfigure}{.49\textwidth}
        \includegraphics[width=\textwidth]{./staticimgs/acceptance/metropolis_half_acceptance.png}
        \caption{Metropolis \( \delta = 1/2 \)}
    \end{subfigure}
    \caption{\label{fig:acceptance} accettanza della mossa Metropolis in funzione di \( \eta \) in plot bi-logaritmico.}
\end{figure}

\iffalse
\appendix
\section{Datasets}

\noindent Datasets per la stima delle correlazioni:

\begin{itemize}
    \item \texttt{corr\_metropolis\_eta}: Metropolis con \( \delta = \sqrt{\eta} \), \( \beta = 10 \), 100 valori di \( \eta \in [0.001, 0.25] \) in progressione geometrica, \( N\ped{step} = 500000 \), \( T\ped{sample} = 1 \)
    \item \texttt{corr\_metropolis\_half}: Metropolis con \( \delta = \frac{1}{2} \), \( \beta = 10 \), 100 valori di \( \eta \in [0.001, 0.25] \) in progressione geometrica, \( N\ped{step} = 500000 \), \( T\ped{sample} = 1 \)
    \item \texttt{corr\_cut\_eta}: sarto + Metropolis con \( \delta = \sqrt{\eta} \), \( p\ped{cut} = 0.06 \), \( \beta = 10 \), 100 valori di \( \eta \in [0.001, 0.25] \) in progressione geometrica, \( N\ped{step} = 500000 \), \( T\ped{sample} = 1 \)
    \item \texttt{metropolis\_eta}: Metropolis con \( \delta = \sqrt{\eta} \), \( \beta = 10 \), 48 valori di \( \eta \in [0.001, 0.25] \) in progressione geometrica, \( N\ped{step} = 6 \times 10^{6} \), \( T\ped{sample} = 5 \)
    \item \texttt{corr\_metropolis\_half}: Metropolis con \( \delta = \frac{1}{2} \), \( \beta = 10 \), 48 valori di \( \eta \in [0.001, 0.25] \) in progressione geometrica, \( N\ped{step} = 6 \times 10^{6} \), \( T\ped{sample} = 5 \)
    \item \texttt{corr\_cut\_eta}: sarto + Metropolis con \( \delta = \sqrt{\eta} \), \( p\ped{cut} = 0.06 \), \( \beta = 10 \), 48 valori di \( \eta \in [0.001, 0.25] \) in progressione geometrica, \( N\ped{step} = 6 \times 10^{6} \), \( T\ped{sample} = 5 \)
\end{itemize}

\iffalse % SIMULAZIONI A TEMPO DINAMICO
\begin{itemize}
    \item \texttt{metropolis\_eta}: Metropolis con \( \delta = \sqrt{\eta} \), \( \beta = 10 \), \( 30 \) valori di \( \eta \in [0.008, 0.25] \) in progressione geometrica, \( N\ped{step}(\eta) = 5000 \cdot {(0.25/ \eta)}^{1.9} \), \( T\ped{sample} = 5 \)
    \item \texttt{metropolis\_half}: Metropolis con \( \delta = 1/2 \), \( \beta = 10 \), \( 36 \) valori di \( \eta \in [0.004, 0.25] \) in progressione geometrica, \( N\ped{step}(\eta) = 5000 \cdot {(0.25/ \eta)}^{2.04} \), \( T\ped{sample} = 5 \)
    \item \texttt{cut\_eta}: Metropolis con \( \delta = \sqrt{\eta} \), \( \beta = 10 \), \( 48 \) valori di \( \eta \in [0.001, 0.25] \) in progressione geometrica, \( N\ped{step}(\eta) = 5000 \cdot {(0.25/ \eta)}^{1.32} \), \( T\ped{sample} = 5 \)
\end{itemize}
\fi
\fi

\printbibliography{}

\end{document}