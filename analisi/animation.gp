set terminal gif animate delay 3 size 1200,720
set output outgif

set datafile separator ","
stats fname matrix nooutput

set xrange [0:int(STATS_columns)]
set yrange [0:1]

do for [j=0:(int(STATS_size_y)-1)] {
    set title 'time '.j
    plot fname matrix using 1:3 every :::j::j with lines title "y(t)"
}