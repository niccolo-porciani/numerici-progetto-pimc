#!/usr/bin/env python
"""Plotta la var(Q)/beta (valore atteso 1) e fa l'estrapolazione al continuo con fit parabolico
Argomenti da linea di comando:
- Nome del file prodotto da analysis.py
- Tipo di algoritmo usato per la scelta del limiti del fit ('metropolis_eta', 'metropolis_half', 'cut_eta')
"""

import sys
import numpy as np
import matplotlib.pyplot as plt
from uncertainties import ufloat_fromstr, correlated_values
from scipy.optimize import curve_fit

import utils

plt.style.use('style.yaml')


def constant(x, a):
    return np.full_like(x, a)


def quadratic(x, a, b):
    return a + b * x**2


alg = {
    'metropolis_eta': {'limits': (0.009, 0.06), 'function': quadratic},
    'metropolis_half': {'limits': (0.006, 0.06), 'function': quadratic},
    'cut_eta': {'limits': (0, 0.023), 'function': constant}
}

N, eta, beta, Q_avg_mod, Q_std_dev = np.loadtxt(
    sys.argv[1], unpack=True, dtype=object,
    converters={0: int, 1: float, 2: float, 3: float, 4: lambda col_bytes: ufloat_fromstr(col_bytes.decode("latin1"))}
)

var_Q_norm = Q_std_dev**2 / beta.astype(float)
plt.errorbar(
    eta.astype(float), [sq.n for sq in var_Q_norm],
    yerr=[sq.s for sq in var_Q_norm],
    linestyle='', marker='.', color='blue', markersize=2
)

# Fit
which = sys.argv[2]
fit_limits = alg[which]['limits']
fit_func = alg[which]['function']

mask = (fit_limits[0] <= eta) & (eta <= fit_limits[1])

x = eta[mask].astype(float)
y = var_Q_norm[mask]
yn = np.array([_.n for _ in y])
ys = np.array([_.s for _ in y])

opt, cov = curve_fit(fit_func, x, yn, sigma=ys, absolute_sigma=True)
chi2 = utils.chisq(x, yn, ys, fit_func, opt)

print(f'{correlated_values(opt, cov)} \t chi^2 / ndof = {chi2 / (len(y) - len(opt))}')

plt.errorbar(x, yn, ys, linestyle='', marker='.', color='red', markersize=2)

x_fit = np.linspace(0, 0.105, num=100)
plt.plot(x_fit, fit_func(x_fit, *opt), color='black', linewidth=0.5)
plt.xscale('log')

plt.ylabel(r'Var$(Q) / \beta$')
plt.xlabel(r'$\eta$')
plt.savefig(f'img/{which}_var_Q.png')
plt.show()
