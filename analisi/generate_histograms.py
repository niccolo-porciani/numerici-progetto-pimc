#!/usr/bin/env python

import view
import glob

metro_etas = ["0.250000", "0.009662", "0.008382", "0.001004"]
half_etas = ["0.250000", "0.006317", "0.005482", "0.001004"]
cut_etas = ["0.250000", "0.009662", "0.005482", "0.001004"]

if __name__ == "__main__":
    for etas, name in zip([metro_etas, half_etas, cut_etas], ["metro", "half", "cut"]):
        for eta in etas:
            view.view(glob.glob(f"../simulazioni/saves/6M/out_{name}/pimc_{eta}_*")[0],
                      f"./imgs/istogrammi/istogramma_6M_{name}_{float(eta):.3f}.png",
                      correlations=False, show=False, xlims=[-15,15])