#!/usr/bin/env python
"""
Carica misure e scrive pickle
Argomenti da linea di comando:
 - Nome del file pickle
 - Directory o lista di file con i dati da caricare
"""

import sys
import pickle

from parallel_computation import parallel_load_measurements

OUT_PICKLE = sys.argv[1]
DIR_OR_LIST = sys.argv[2:]

print("Loading measurements...")
data = parallel_load_measurements(DIR_OR_LIST)

with open(OUT_PICKLE, 'wb') as f:
    pickle.dump(data, f)
