#!/usr/bin/env python
# Argomenti da linea di comando
#  - File dati su cui ottimizzare
#  - Nome della figura da salvare

import sys
import numpy as np
import matplotlib.pyplot as plt
from cmath import rect

import utils

plt.style.use("style.yaml")

Q, K = np.loadtxt(sys.argv[1], unpack=True, delimiter=',')
params = utils.get_parameters(sys.argv[1])

BLOCK_SIZE_MAX = 50000
print(len(K), BLOCK_SIZE_MAX)

fig = utils.block_optimize(K, BLOCK_SIZE_MAX, return_fig=True)
ax = fig.axes[0]
ax.set_xlabel("Block size")
ax.set_ylabel(r"$\sigma_E$")
# ax.set_xscale("log")
# ax.set_yscale("log")
# ax.axvline(4000, linestyle='--', color="grey")
plt.figure(fig)
plt.savefig(sys.argv[2])
plt.close()