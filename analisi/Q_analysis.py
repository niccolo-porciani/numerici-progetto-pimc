#!/usr/bin/env python
"""
Calcola il valore medio di |Q| e la varianza di Q (con errore fatto con bootstrap) e li scrive su file
Argomenti da linea di comando:
- Dimensione dei blocchi per bootstrap con binning
- Pickle con le misure
- File txt in cui mettere l'output
"""

import sys
import pickle
import numpy as np
from uncertainties import ufloat

from parallel_computation import parallel_compute_observables
import utils

N_RESAMPLE = 100
BLOCK_SIZE = int(sys.argv[1])


def compute_observables(measurement, observables):
    """Compute the variance of the topological charge (error esitmate via
    bootstrap with binning) and the average of its absolute value
    """
    N, eta, beta, Q, K = measurement.transpose()

    Q_avg_mod = np.average(np.abs(Q))
    Q_std_dev = np.std(Q)

    Q_std_dev = utils.std(Q)
    err_Q_std_dev = utils.std(utils.bootstrap(Q, utils.std, N_RESAMPLE, None, BLOCK_SIZE))

    observables.append(
        (N, eta, beta, Q_avg_mod, ufloat(Q_std_dev, err_Q_std_dev))
    )


struc_type = [
    ('N', int), ('eta', float), ('beta', float), ('Q_avg_mod', float), ('Q_std_dev', object)
]

with open(sys.argv[2], 'rb') as f:
    meas = pickle.load(f)
obs = parallel_compute_observables(meas, compute_observables, struc_type)

np.savetxt(sys.argv[3], obs, fmt='%r')
