#!/usr/bin/env python
"""
Calcola le correlazioni temporali delle osservabili K e Q
Argomenti da linea di comando:
 - File pickle generato da meas_to_pickle.py
 - Nome del file in cui salvare i tempi di correlazione
"""

import sys
import pickle
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

import utils
from parallel_computation import parallel_compute_observables


C_LIMIT = 0.005
T_LIMIT = 1000


def exponential(x, a, tau):
    return a * np.exp(-x/tau)


def correlation_time(x, label=None):
    """Compute the montecarlo correlation time via an exponential fit
    """
    max_time = int(len(x)/20)
    C = utils.connected_time_correlation(x, max_time, normalized=True)

    stop = np.argmin(C > C_LIMIT)
    if stop == 0:
        stop = T_LIMIT
    elif stop == 1:
        stop = 3

    C_fit = C[:stop]

    opt, cov = curve_fit(exponential, np.arange(stop), C_fit, p0=(1, 1))

    # Plots
    # plt.plot(range(max_time), C, color='red', label=label)
    # t_plot = np.linspace(0, stop, num=100)
    # plt.plot(t_plot, exponential(t_plot, *opt))
    # plt.legend()
    # plt.show()

    return opt[1]


def compute_correlation_time(measurement, observables):
    N, eta, beta, Q, K = measurement

    observables.append(
        (N, eta, beta, correlation_time(Q, 'Q'), correlation_time(K, 'K'))
    )


with open(sys.argv[1], 'rb') as f:
    meas = pickle.load(f)

obs = parallel_compute_observables(
    meas, compute_correlation_time,
    [('N', int), ('eta', float), ('beta', float), ('tau_Q', float), ('tau_K', float)]
)

eta = obs['eta']
tau_K = obs['tau_K']
tau_Q = obs['tau_Q']

np.savetxt(
    sys.argv[2], np.column_stack((eta, tau_K, tau_Q)), header='# eta tau_K tau_Q'
)
