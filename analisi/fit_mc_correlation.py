#!/usr/bin/env python
"""
Plotta i tempi di correlazione vs eta e fitta con una retta log-log quelli di K
Argomenti da linea di comando
 - File generato da obs_mc_correlation.py
 - Prefisso per le figure
"""

import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from uncertainties import correlated_values, umath

plt.style.use("style.yaml")
fig_prefix = sys.argv[2]
eta, tau_K, tau_Q = np.loadtxt(sys.argv[1], unpack=True)

# Fit correlations times of the kinetic energy with a line in log scale
# def retta(x, a, b):
#     return a + b*x


# opt, cov = curve_fit(retta, np.log(eta), np.log(tau_K))

# a, b = correlated_values(opt, cov)
# print(f'fit tau_E = C eta^alpha \t C = {umath.exp(a):.1u} \t alpha = {b:.1u}')

# Plots
# x = np.linspace(eta.min(), eta.max(), num=100)
# y = np.exp(retta(np.log(x), *opt))

plt.plot(eta, tau_K, marker='.', color='r', linestyle='')
# plt.plot(x, y, color='k')
plt.xlabel('$\\eta$')
plt.ylabel('$\\tau_{E}$')
plt.xscale('log')
plt.yscale('log')
plt.savefig(fig_prefix + "_E.png")
plt.show()

plt.plot(eta, tau_Q, marker='.', color='b', linestyle='')
plt.xlabel('$\\eta$')
plt.ylabel('$\\tau_{Q}$')
plt.xscale('log')
plt.yscale('log')
plt.savefig(fig_prefix + "_Q.png")
plt.show()