#!/usr/bin/env python

import sys
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
from scipy.stats import norm

import utils

plt.style.use("style.yaml")
mpl.rcParams['agg.path.chunksize'] = 10000

def view(file_path, hist_img_path: str = None, correlations: bool = True,
         show: bool = True, xlims = None):
    params = utils.get_parameters(file_path)
    Q, K = np.loadtxt(file_path, unpack=True, delimiter=',')

    title = f'$N$: ${params["N"]}$, $\\eta$: ${params["eta"]}$, $\\beta$: ${params["beta"]}$, cut\\_prob: ${params["cut_prob"]}$'

    # Montecarlo history plot
    plt.plot(range(len(Q)), Q, linewidth=0.2, color='blue')
    plt.title(title)
    plt.ylabel(r'$Q$')
    plt.xlabel(r'$t_{MC}$')
    if show: plt.show()
    plt.close()

    plt.plot(range(len(K)), K, linewidth=0.2, color='red')
    plt.title(title)
    plt.ylabel(r'$K$')
    plt.xlabel(r'$t_{MC}$')
    if show: plt.show()
    plt.close()

    # Montecarlo correlation plot
    if correlations:
        max_time = int(len(Q)/200)
        C_Q = utils.connected_time_correlation(Q, max_time, normalized=True)
        plt.plot(range(max_time), C_Q, linewidth=0.5, color='blue')
        plt.title(title)
        plt.ylabel('$C_Q(t_{MC}) / C_Q(0)$')
        plt.xlabel('$t_{MC}$')
        plt.show()
        plt.close()

        max_time = int(len(K)/200)
        C_K = utils.connected_time_correlation(K, max_time, normalized=True)
        plt.plot(range(max_time), C_K, linewidth=0.5, color='red')
        plt.title(title)
        plt.ylabel('$C_K(t_{MC}) / C_K(0)$')
        plt.xlabel('$t_{MC}$')
        plt.show()
        plt.close()

    # Histograms
    bins = np.arange(Q.min(), Q.max()+2)
    bins = bins - 0.5
    plt.hist(Q, bins, density=True, histtype='step', fill=False, color='b', label=r'Istogramma di $Q$')

    if xlims is not None:
        plt.xlim(xlims)
        x = np.linspace(*xlims,1000)
    else:
        x = np.linspace(Q.min() - 2, Q.max() + 2, 1000)
    plt.plot(x, norm.pdf(x, 0, np.sqrt(params["beta"])), color='g', label=r'PDF attesa')

    plt.xlabel(r'$Q$')
    plt.ylabel(r'$P(Q)$')
    plt.title(title)
    # plt.legend()
    if hist_img_path is not None:
        plt.savefig(hist_img_path)
    if show: plt.show()

    # Maximum likehood test della gaussianità
    mean, std = norm.fit(Q)
    Q_occ, bins = np.histogram(Q, bins)
    mu = len(Q) * np.array([norm.cdf(bins[i+1], mean, std) - norm.cdf(bins[i], mean, std) for i in range(len(bins)-1)])

    not_zero = Q_occ != 0
    likehood = 2 * np.sum(Q_occ[not_zero] * np.log(Q_occ[not_zero] / mu[not_zero]))
    print(f'cut_prob = {params["cut_prob"]} \t std / sqrt(beta): {std/np.sqrt(params["beta"])} \t lambda/ndof = {likehood/(len(bins) - 3)}')

if __name__ == "__main__":
    if len(sys.argv) < 3:
        imgpath = None
    else:
        imgpath = sys.argv[2]

    view(sys.argv[1], imgpath)