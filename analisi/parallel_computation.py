import os
import numpy as np
import functools
import multiprocessing
from uncertainties import ufloat

import utils


def load_measurements(fname, measurements):
    """Extract N, eta, beta, topological charge and kinetic energy from file
    """
    Q, K = np.loadtxt(fname, unpack=True, delimiter=',')
    params = utils.get_parameters(fname)
    measurements.append(
        (params['N'], params['eta'], params['beta'], Q, K)
    )


def parallel_load_measurements(fname, func=load_measurements):
    # If fname is a list of files fnames is just the list, if fname is
    # a directory we iterate over files in fname
    if len(fname) == 1:
        if os.path.isdir(fname[0]):
            fnames = [os.path.join(fname[0], f) for f in os.listdir(fname[0])]
        else:
            fnames = fname
    else:
        fnames = fname

    # Multiprocessing
    manager = multiprocessing.Manager()
    meas_shared = manager.list()

    pool = multiprocessing.Pool()
    pool.map(
        functools.partial(func, measurements=meas_shared),
        fnames
    )

    # Put measurements in a structured array and sort first by N, then by eta
    # len_meas = len(meas_shared[0][-1])
    meas = np.array(
        meas_shared,
        dtype=[
            ('N', int), ('eta', float), ('beta', float),
            ('Q', object), ('K', object)
        ]
    )

    idx = np.lexsort((meas['eta'], meas['N']))
    meas = meas[idx]

    return meas


def parallel_compute_observables(measurements, func, struc_type):
    # Multiprocessing
    manager = multiprocessing.Manager()
    obs_shared = manager.list()

    pool = multiprocessing.Pool()
    pool.map(
        functools.partial(
            func,
            observables=obs_shared,
        ),
        measurements
    )

    # Put measurements in a structured array
    obs = np.array(obs_shared, dtype=struc_type)

    return obs
