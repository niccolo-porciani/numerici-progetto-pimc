#!/usr/bin/env python

import sys
import numpy as np
import multiprocessing
import matplotlib.pyplot as plt

import utils

plt.style.use('style.yml')

meas = np.loadtxt(sys.argv[1], delimiter=',', dtype=[('Q', float), ('K', float)])
eta = utils.get_parameters(sys.argv[1])['eta']

which = sys.argv[2]
meas = meas[which]

BLOCK_SIZE_MAX = 1000  # int(len(K) / 4)

blocks = np.arange(1, BLOCK_SIZE_MAX, 100)


def parallel_bootstrap(block_size):
    print(block_size)
    return utils.bootstrap(meas, utils.std, 300, None, block_size)


if __name__ == '__main__':
    with multiprocessing.Pool() as p:
        estimator_vals = p.map(parallel_bootstrap, blocks)

        sigma_esitmator = [utils.std(e_val) for e_val in estimator_vals]

        plt.plot(blocks, sigma_esitmator, color='blue')
        plt.xlabel('block size')
        plt.ylabel(f'Err on $\\sigma_{{{which}}}$')
        plt.title(f'$\\eta = {eta}$')
        plt.savefig(sys.argv[3])
        plt.show()
        plt.close()
