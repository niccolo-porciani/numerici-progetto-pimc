#include <iostream>
#include <cmath>
#include <yaml-cpp/yaml.h>
#include <sys/stat.h>
#include <omp.h>
#include <vector>

#include "particle.hpp"

std::vector<double> linspace(const double start, const double stop, const int size)
{
    std::vector<double> x;
    double delta = (stop - start) / (double) size;

    for (int i = 0; i < size; i++) {
        x.push_back(start + i * delta);
    }

    return x;
}

std::vector<double> inverse_linspace(const double start, const double stop, const int size)
{
    std::vector<double> x;
    double delta = (1./start - 1./stop) / (double) size;

    for (int i = 0; i <= size; i++) {
        x.push_back(1./(1./start - i * delta));
    }

    return x;
}


int * int_linspace(const int start, const int stop, const int step)
{
    const int size = (stop - start) / step;
    int * x = new int [size];
    x[0] = start;
    for (int i = 1; i < size; i++) {
        x[i] = x[i-1] + step;
    }

    return x;
}

std::vector<double> logspace(const double start, const double stop, const int size)
{
    std::vector<double> x;
    double r = stop / start;

    for (int i = 0; i < size; i++) {
        x.push_back(start * pow(r, (double)i / (double) size));
    }

    return x;
}


int main(int, char * argv[])
{
    YAML::Node config = YAML::LoadFile(argv[1]);

    const std::string out_dir = config["out_dir"].as<std::string>();
    const int num_threads = config["num_threads"].as<int>();

    // Particle parameters
    const int N_start = config["particle"]["N_start"].as<int>(),
        N_stop = config["particle"]["N_stop"].as<int>(),
        N_N = config["particle"]["N_N"].as<int>();
    const int N_beta = config["particle"]["N_beta"].as<int>();
    const double beta_start = config["particle"]["beta_start"].as<double>(),
        beta_stop = config["particle"]["beta_stop"].as<double>();

    // Simulation parameters
    const double cut_prob = config["simulation"]["cut_prob"].as<double>();
    const int N_step = config["simulation"]["N_step"].as<int>(),
        sample_freq = config["simulation"]["sample_freq"].as<int>(),
        sample_start = config["simulation"]["sample_start"].as<int>();


    // Create output directory
    mkdir(out_dir.c_str(), 0744);

    std::vector<double> N = inverse_linspace(N_start, N_stop, N_N);
    std::vector<double> beta = logspace(beta_start, beta_stop, N_beta);

    omp_set_num_threads(num_threads);

#pragma omp parallel for schedule(dynamic, 1) collapse(2)
    for (unsigned i = 0; i < N.size(); i++) {
        for (unsigned j = 0; j < beta.size(); j++) {
            particle p_ring((unsigned) N[i], beta[j], {brownian});
            std::cout << "N: " << (unsigned) N[i] << "\teta: " << p_ring.eta << "\tbeta: " << beta[j] << "\tcut_prob: " << cut_prob << "\n";

            p_ring.montecarlo(cut_prob, (unsigned) ((double) N_step / p_ring.eta), sample_freq, sample_start, out_dir);
        }
    }

    return 0;
}
