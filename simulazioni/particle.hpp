#pragma once

#include <iostream>
#include <string>
#include <vector>

#undef NDEBUG
#include <cassert>

enum init_type {constant, brownian, wound};

struct init_param {
    init_type type;
    int Q = 0;
    double y0 = 0.;

    init_param(init_type type_) {
        assert(type_ == brownian);
        type = type_;
    }

    init_param(init_type type_, int Q_) {
        assert(type_ == wound);
        type = type_;
        Q = Q_;
    }

    init_param(init_type type_, double y0_) {
        assert(type_ == constant);
        type = type_;
        y0 = y0_;
    }
};

struct particle {
    double * path;
    unsigned ** path_nn;
    unsigned N;
    double beta;
    double eta;

    // Creatore e distruttore
    particle(unsigned N_, double beta_, init_param init_p);
    ~particle();

    double * init_const(double y0);
    double * init_wound(int Q);
    double * init_browinan(double eta);

    // Calcolo osservabili
    int charge();
    double kinetic();
    // double potential();

    void montecarlo(double cut_prob, int N_step, int sample_freq, int sample_start, const std::string& out_dir);
};

double distance (double y, double x);
