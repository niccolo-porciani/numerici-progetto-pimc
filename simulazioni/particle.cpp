#include "particle.hpp"

#include <cmath>
#include <random>
#include <string>
#include <fstream>
#include <iomanip>

#include "pcg-cpp/include/pcg_random.hpp"

inline unsigned modulo(int a, int b)
{
    return ((a % b) + b) % b;
}


double distance (double y, double x)
{
    double diff = y - x;
    if (diff > 0.5)
        return diff - 1;
    else if (diff < -0.5)
        return 1 + diff;
    else
        return diff;
    // return y - x - round(y - x);
}

double fractional_part(double x)
{
    if (x > 1) {
        return fractional_part(x-1);
    } else if (x < 0) {
        return fractional_part(x+1);
    }

    return x;
}


double * particle::init_browinan(double eta)
{
    pcg_extras::seed_seq_from<std::random_device> seed_source;
    pcg32 gen(seed_source);

    std::normal_distribution<double> rand_normal(0, sqrt(eta));

    double * y = new double[N];

    y[0] = 0;
    for (unsigned i = 1; i < N; i++) {
        y[i] = y[i-1] + rand_normal(gen);
    }

    for (unsigned i = 0; i < N; i++) {
        y[i] = fractional_part(y[i]);
    }

    return y;
}

double * particle::init_wound(int Q)
{
    double * y = new double[N];
    for (unsigned i = 0; i < N; i++) {
        y[i] = fractional_part(((double) i * Q) / (double) N);
    }

    return y;
}

double * particle::init_const(double y0)
{
    double * y = new double[N];
    for (unsigned i = 0; i < N; i++) {
        y[i] = fractional_part(y0);
    }

    return y;
}

particle::particle (unsigned N_, double beta_, init_param init_p)
{
    N = N_;
    beta = beta_;

    eta = beta / (double) N;

    path_nn = new unsigned * [N];

    // Alloca e inizializza paths, inzializza etas
    switch (init_p.type) {
    case constant: {
        path = init_const(init_p.y0);
        break;
    }
    case brownian: {
        path = init_browinan(eta);
        break;
    }
    case wound: {
        path = init_wound(init_p.Q);
        break;
    }
    default:
        break;
    }

    // Alloca e inizializza path_nn
    for (unsigned i = 0; i < N; i++) {
        path_nn[i] = new unsigned[2]{ modulo(i - 1, N), modulo(i + 1, N) };
    }
}

particle::~particle ()
{
    delete [] path;

    for (unsigned i = 0; i < N; i++) {
        delete [] path_nn[i];
    }
    delete [] path_nn;
}

int particle::charge ()
{
    double result = 0.;

    for (unsigned i = 0; i < N; i++) {
        result += distance(path[modulo(i+1, N)], path[i]);
    }

    return (int) round(result);
}

double particle::kinetic()
{
    double result = 0;

    for (unsigned j = 0; j < N; j++) {
        result += pow((path[(j + 1) % N] - path[j]), 2);
    }

    return 0.5 * result / eta / eta / (double)N;
}


void particle::montecarlo(double cut_prob, int N_step, int sample_freq, int sample_start, const std::string& out_dir)
{
    // Generatore casuale
    pcg_extras::seed_seq_from<std::random_device> seed_source;
    pcg32 gen(seed_source);

    // Distribuzioni necessarie
    std::uniform_int_distribution<unsigned> rand_site(0, N - 1);
    std::uniform_real_distribution<double> rand_double(0, 1);

    // Valori di delta
    const double delta = sqrt(eta);
    const double epsilon = 0.02*eta;

    const int N_sample = (N_step - sample_start) / sample_freq
        + ((N_step - sample_start) % sample_freq != 0 ? 1 : 0);

    int * Q = new int[N_sample];
    double * K = new double[N_sample];

    unsigned curr_idx = 0;

    unsigned accepted_metropolis = 0, accepted_cut = 0, proposed_cut = 0;

    // std::ofstream file_path;
    // file_path.open(out_dir + "/path_"
    //                + std::to_string(N) + "_"
    //                + std::to_string(beta) + "_"
    //                + std::to_string(cut_prob) + ".csv");

    // // Metadata
    // file_path << std::fixed << std::setprecision(10);
    // file_path << "# N: " << N << "\n"
    //           << "# eta: " << eta <<  "\n"
    //           << "# beta: " << beta << "\n"
    //           << "# N_step: " << N_step << "\n"
    //           << "# sample_start: " << sample_start << "\n"
    //           << "# sample_freq: " << sample_freq << "\n"
    //           << "# cut_prob: " << cut_prob << "\n";

    // Main time loop
    for (int t = 0; t < N_step; t++) {
        if ((cut_prob >= 0) && (rand_double(gen) < cut_prob)) {
            unsigned i_cut = 0;
            double y0 = path[0]; // Initial position
            double y_r = fractional_part(y0 + 0.5);

            bool condition = false;

            while (i_cut < N && not condition) {
                condition = fabs(distance(path[i_cut], y_r)) <= epsilon;
                i_cut++;
            }

            if (condition) {
                i_cut--;
                // i_cut is the minimum inder for which |d(y_{i_cut}, y_r)| < epsilon
                double d_old = distance(path[i_cut], path[path_nn[i_cut][0]]); // d(y_i, y_{i-1})
                double d_new = distance(fractional_part(2*y_r - path[i_cut]), path[path_nn[i_cut][0]]); // d(y_i', y_{i-1})
                double DeltaSE = (d_new*d_new - d_old*d_old) / (2*eta);

                double acc = exp(-DeltaSE);

                // std::cout << DeltaSE << "\n";

                proposed_cut++;
                if ((acc > 1) || (rand_double(gen) < acc)) {
                    accepted_cut++;
                    for (unsigned i = i_cut; i < N; i++) {
                        path[i] = fractional_part(2 * y_r - path[i]);
                    }
                }

                // for (unsigned i = i_cut; i < N; i++) {
                //     path[i] = fractional_part(2 * y_r - path[i]);
                // }
            }
        }
        // Local Metropolis
        // Loop over single path
        for (unsigned i = 0; i < N; i++){
            unsigned i_curr = rand_site(gen);

            double y_old = path[i_curr];
            double y_before = path[path_nn[i_curr][0]], y_after = path[path_nn[i_curr][1]];

            double y_new = fractional_part(y_old + delta * (2 * rand_double(gen) - 1));

            double DeltaSE = (1./(2*eta)) * ( pow(distance(y_after, y_new), 2) + pow(distance(y_new, y_before), 2) - pow(distance(y_after, y_old), 2) - pow(distance(y_old, y_before), 2));

            double acc = exp(-DeltaSE);

            if ((acc > 1) || (rand_double(gen) < acc)) {
                accepted_metropolis++;
                path[i_curr] = y_new;
            }
        }

        // Misure
        if (t >= sample_start && (t - sample_start) % sample_freq == 0) {
            Q[curr_idx] = charge();
            K[curr_idx] = kinetic();
            curr_idx++;
        }
    }

    std::ofstream file;
    file.open(out_dir + "/pimc_"
              + std::to_string(eta) + "_"
              + std::to_string(beta) + "_"
              + std::to_string(cut_prob) + ".csv");

    // Metadata
    file << std::fixed << std::setprecision(10);
    file << "# N: " << N << "\n"
         << "# eta: " << eta <<  "\n"
         << "# beta: " << beta << "\n"
         << "# N_step: " << N_step << "\n"
         << "# sample_start: " << sample_start << "\n"
         << "# sample_freq: " << sample_freq << "\n"
         << "# cut_prob: " << cut_prob << "\n"
         << "# acceptance_metropolis: " << (double) accepted_metropolis / (double) N_step / (double) N << "\n"
         << "# acceptance_cut: " << (double) accepted_cut / (double) proposed_cut << "\n";

    // Data
    for (int i = 0; i < N_sample; i++) {
        file << Q[i] << "," << K[i] << "\n";
    }
    file.close();
}
